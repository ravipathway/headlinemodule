﻿/*
' Copyright (c) 2017 Christoc.com
'  All rights reserved.
' 
' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
' TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
' THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
' CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
' DEALINGS IN THE SOFTWARE.
' 
*/
using System.Collections.Generic;
using DotNetNuke.Data;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;

namespace NRNA.Modules.HeadLinesModule.Components
{
    class HeadlineController
    {
        public void CreateItem(Item t)
        {
            using (IDataContext ctx = DataContext.Instance())
            {
                var rep = ctx.GetRepository<Item>();
                rep.Insert(t);
            }
        }

        public void DeleteItem(int itemId, int moduleId)
        {
            var t = GetItem(itemId, moduleId);
            DeleteItem(t);
        }

        public void DeleteItem(Item t)
        {
            using (IDataContext ctx = DataContext.Instance())
            {
                var rep = ctx.GetRepository<Item>();
                rep.Delete(t);
            }
        }

        public IEnumerable<Item> GetItems(int moduleId)
        {
            IEnumerable<Item> t;
            using (IDataContext ctx = DataContext.Instance())
            {
                var rep = ctx.GetRepository<Item>();
                t = rep.Get(moduleId);
            }
            return t;
        }

        public Item GetItem(int itemId, int moduleId)
        {
            Item t;
            using (IDataContext ctx = DataContext.Instance())
            {
                var rep = ctx.GetRepository<Item>();
                t = rep.GetById(itemId, moduleId);
            }
            return t;
        }

        public void UpdateItem(Item t)
        {
            using (IDataContext ctx = DataContext.Instance())
            {
                var rep = ctx.GetRepository<Item>();
                rep.Update(t);
            }
        }

        private string getConnectionString()
        {
            return ConfigurationManager.ConnectionStrings["SiteSqlServer"].ConnectionString.ToString();
        }

        public DataTable GetLatestHeadlines(int portalId)
        {
            SqlConnection con = new SqlConnection(getConnectionString());
            con.Open();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            var query = @"select top 2  'news/newsid/'+ Convert(varchar(200),NewsId) as URL, NewsTitle as Headlines,NewsDate as Date from NRNANewsItemModule where PortalId=@0 union all
                          select top 2  'Events/eventid/'+ Convert(varchar(200),EventId) as URL,EventTitle as Headlines,EventStartDateTime as Date from EventsItemModule where PortalId = @0  union all
                          select top 2  'Notices/'+ Convert(varchar(200),NoticeId) as URL,NoticeTitle as Headlines,PostedDate as Date from NoticeModule_Items where PortalId = @0 union all
                          select top 2  'press-release/prid/'+ Convert(varchar(200),PressReleaseId) as URL,PressReleaseTitle as Headlines,PressReleaseDate as Date from PressReleaseItemModule where PortalId = @0  union all
                          select top 2  'Publications/PubId/'+ Convert(varchar(200),pubId) as URL,PublicationTitle as Headlines,PublicationDate as Date from NRNAPublicationModule where PortalId = @0 union all
                          select top 2   ContentImageUrl as URL,ContentTitle as Headlines,ContentStartDate as Date from NRNAHomePageLayoutManagementModule where PortalId = @0 order by Date Desc";

            cmd.Parameters.AddWithValue("@0", portalId);
            cmd.CommandText = query;
            SqlDataAdapter da = new SqlDataAdapter(cmd);

            DataTable dt = new DataTable();
            da.Fill(dt);
            cmd.ExecuteNonQuery();
            con.Close();
            //using (IDataContext ctx = DataContext.Instance())
            //{
            //    dt = ctx.ExecuteQuery<DataTable>(System.Data.CommandType.Text, @"SELECT TOP 3 * FROM [NRNAPublicationModule] WHERE (ShowInICCNCC = 2) OR ([PortalId] = @0) ORDER BY PublicationDate DESC", portalId);
            //}
            return dt;
        }

    }
}
