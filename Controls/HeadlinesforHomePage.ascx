﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="HeadlinesforHomePage.ascx.cs" Inherits="NRNA.Modules.HeadLinesModule.Controls.HeadlinesforHomePage" %>
<div id="dnn_ctr2703_ModuleContent" class="DNNModuleContent ModHeadLinesModuleC">
    <header>
        <strong class="title">Headlines</strong>
    </header>
    <div class="slideset">
        <asp:Repeater ID="rptItemList" runat="server">
            <ItemTemplate>
                <p class="slide">
                    <a href='<%# DataBinder.Eval(Container.DataItem, "URL").ToString() %>'><%# DataBinder.Eval(Container.DataItem, "Headlines").ToString() %></a>
                </p>

            </ItemTemplate>
        </asp:Repeater>
    </div>
</div>
