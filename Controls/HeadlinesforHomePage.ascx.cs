﻿using DotNetNuke.Services.Exceptions;
using NRNA.Modules.HeadLinesModule.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace NRNA.Modules.HeadLinesModule.Controls
{
    public partial class HeadlinesforHomePage : HeadLinesModuleModuleBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                var tc = new HeadlineController();
                rptItemList.DataSource = tc.GetLatestHeadlines(PortalId);
                rptItemList.DataBind();
                
            }
            catch (Exception exc) //Module failed to load
            {
                Exceptions.ProcessModuleLoadException(this, exc);
            }
        }
    }
}